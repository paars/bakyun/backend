package middleware

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/rs/zerolog"
)

// ZeroLogger stores the zerolog.Logger instance
type ZeroLogger struct {
	logger zerolog.Logger
}

// NewLogEntry creates a new LogEntry struct for logging
func (l *ZeroLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	ctx := l.logger.With()

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		ctx = ctx.Str("req_id", reqID)
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	ctx = ctx.Str("scheme", scheme).Str("proto", r.Proto).Str("method", r.Method).
		Str("remote", r.RemoteAddr).Str("user-agent", r.UserAgent()).
		Str("host", r.Host).Str("uri", r.RequestURI)

	logger := ctx.Logger()

	logger.Debug().Msg("request started")

	entry := &ZeroLoggerEntry{Logger: logger}
	return entry
}

// ZeroLoggerEntry is a single entry from the ZeroLogger
type ZeroLoggerEntry struct {
	Logger zerolog.Logger
}

func (l *ZeroLoggerEntry) Write(status, bytes int, elapsed time.Duration) {
	l.Logger.Info().Int("status", status).Int("len", bytes).Dur("ms", elapsed).Msg("request complete")
}

// Panic changes the logger scope with the panic error attached
func (l *ZeroLoggerEntry) Panic(v interface{}, stack []byte) {
	if err, ok := v.(error); ok {
		l.Logger = l.Logger.With().Err(err).Str("stack", string(stack)).Logger()
	} else {
		l.Logger = l.Logger.With().Str("panic", fmt.Sprintf("%+v", v)).Str("stack", string(stack)).Logger()
	}
}

// NewZeroLogger creates a new logging handler using a zerolog.Logger
func NewZeroLogger(logger zerolog.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&ZeroLogger{logger})
}

// GetZerologger gets the current logger configured for this request
func GetZerologger(r *http.Request) zerolog.Logger {
	entry := middleware.GetLogEntry(r).(*ZeroLoggerEntry)
	return entry.Logger
}
