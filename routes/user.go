package routes

import (
	"github.com/cking/reverse"
)

func init() {
	reverse.Add("user-profile", "/@{username}", paramUsername)
}

// UserProfile returns the user profile url
func UserProfile(username string) string {
	return reverse.Rev("user-profile", username)
}
