package routes

import (
	"github.com/cking/reverse"
)

// The different activitypub routes
const (
	ActivityPubSharedInbox = "ap-shared-inbox"
)

func init() {
	reverse.Add("ap-users-profile", "/users/{username}", paramUsername)
	reverse.Add("ap-users-profile-inbox", "/users/{username}/inbox", paramUsername)
	reverse.Add("ap-users-profile-outbox", "/users/{username}/outbox", paramUsername)
	reverse.Add("ap-users-profile-follower", "/users/{username}/follower", paramUsername)
	reverse.Add("ap-users-profile-following", "/users/{username}/following", paramUsername)

	reverse.Add(ActivityPubSharedInbox, "/instance/inbox")
}

// ActivityPubUserProfile returns the activitypub specific user profile
func ActivityPubUserProfile(username string) string {
	return reverse.Rev("ap-users-profile", username)
}

// ActivityPubUserProfileInbox is the path to the user profiles inbox
func ActivityPubUserProfileInbox(username string) string {
	return reverse.Rev("ap-users-profile-inbox", username)
}

// ActivityPubUserProfileOutbox is the path to the user profiles outbox
func ActivityPubUserProfileOutbox(username string) string {
	return reverse.Rev("ap-users-profile-outbox", username)
}

// ActivityPubUserProfileFollower is the path to the user profiles followers list
func ActivityPubUserProfileFollower(username string) string {
	return reverse.Rev("ap-users-profile-follower", username)
}

// ActivityPubUserProfileFollowing is the path to the user profiles following list
func ActivityPubUserProfileFollowing(username string) string {
	return reverse.Rev("ap-users-profile-following", username)
}

// ActivityPub
