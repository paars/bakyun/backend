package helper

import (
	"fmt"
	"time"

	diodes "code.cloudfoundry.org/go-diodes"
	"github.com/fatih/color"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/diode"
	"github.com/rs/zerolog/log"
)

// NewLogger creates a new zerolog.Logger instance
func NewLogger(diodeSize int) zerolog.Logger {
	writer := zerolog.ConsoleWriter{
		Out:     color.Output,
		NoColor: false,
	}

	dw := diode.NewWriter(writer, diodes.NewManyToOne(diodeSize, diodes.AlertFunc(func(missed int) {
		fmt.Printf("Logger dropped %d messages", missed)
	})), 10*time.Millisecond)

	return log.Output(dw)
}
