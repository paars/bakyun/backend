package helper

import (
	"net/http"
	"net/url"
)

// FullURL returns the FQDN for the given request
func FullURL(r *http.Request) *url.URL {
	url := *r.URL

	if url.Scheme == "" {
		switch {
		case r.TLS != nil,
			r.Header.Get("X-Forwarded-Proto") == "https",
			r.Header.Get("X-Forwarded-SSL") == "on",
			r.Header.Get("X-Forwarded-HTTPS") == "on":
			url.Scheme = "https"
		default:
			url.Scheme = "http"
		}
	}

	if url.Host == "" {
		host := r.Host
		if r.Header.Get("X-Forwarded-Host") != "" {
			host = r.Header.Get("X-Forwarded-Host")
		}

		url.Host = host
	}

	return &url
}
