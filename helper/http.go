package helper

import (
	"net/http"
	"net/url"
)

func Redirect(w http.ResponseWriter, r *http.Request, uri *url.URL, statusCode ...int) {
	redirectStatusCode := http.StatusFound
	if len(statusCode) > 0 {
		redirectStatusCode = statusCode[0]
	}

	newURL := FullURL(r).ResolveReference(uri).String()

	http.Redirect(w, r, newURL, redirectStatusCode)
}

func RedirectHandler(uri *url.URL, statusCode ...int) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Redirect(w, r, uri, statusCode...)
	})
}
