package helper

import (
	"encoding/base32"
	"net/http"
	"strings"

	"github.com/gorilla/securecookie"

	"github.com/gorilla/sessions"

	"gitlab.com/paars/bakyun/redix"
)

// default values for several storage options
const (
	DefaultPrefix      = "go:session:"
	DefaultSessionName = "sess"
	DefaultMaxAge      = 30 * 24 * 60 * 60
)

const cookieKeyLength = 32

// RadixStore represents the currently configured session session store. It is essentially
// a wrapper around a Redigo connection pool.
type RadixStore struct {
	prefix      string
	sessionName string
	redis       *redix.Redix

	Options *sessions.Options
	Codecs  []securecookie.Codec
}

// NewRadixStore returns a new RadixStore instance. The pool parameter should be a pointer to a
// Radix connection pool.
func NewRadixStore(prefix string, sessionName string, redis *redix.Redix, keyPairs ...[]byte) *RadixStore {
	store := &RadixStore{prefix: prefix, redis: redis, sessionName: sessionName}
	if prefix == "" {
		store.prefix = DefaultPrefix
	}
	if sessionName == "" {
		store.sessionName = DefaultSessionName
	}
	store.Options = &sessions.Options{
		Path:   "/",
		MaxAge: DefaultMaxAge,
	}
	store.Codecs = securecookie.CodecsFromPairs(keyPairs...)

	return store
}

// Get returns a session by the given token
func (s *RadixStore) Get(r *http.Request, token string) (*sessions.Session, error) {
	registry := sessions.GetRegistry(r)
	return registry.Get(s, token)
}

// New loads or creates a new session for the token
func (s *RadixStore) New(r *http.Request, token string) (*sessions.Session, error) {
	session := sessions.NewSession(s, token)

	opts := *s.Options
	session.Options = &opts

	session.IsNew = true

	var err error

	if c, errCookie := r.Cookie(token); errCookie == nil {
		err = securecookie.DecodeMulti(token, c.Value, &session.ID, s.Codecs...)
		if err == nil {
			err = s.loadSession(session)
			if err == nil {
				session.IsNew = false
			}
		}
	}

	return session, err
}

// Save the session to redis
func (s *RadixStore) Save(_ *http.Request, w http.ResponseWriter, session *sessions.Session) error {
	// TODO: make session expire in redis if max age == 0
	if session.Options.MaxAge < 0 {
		if err := s.eraseSession(session); err != nil {
			return err
		}
		http.SetCookie(w, sessions.NewCookie(session.Name(), "", session.Options))
		return nil
	}

	if session.ID == "" {
		session.ID = strings.TrimRight(base32.StdEncoding.EncodeToString(securecookie.GenerateRandomKey(cookieKeyLength)), "=")
	}

	if err := s.saveSession(session); err != nil {
		return err
	}

	encoded, err := securecookie.EncodeMulti(session.Name(), session.ID, s.Codecs...)
	if err != nil {
		return err
	}

	http.SetCookie(w, sessions.NewCookie(session.Name(), encoded, session.Options))
	return nil
}

func (s *RadixStore) saveSession(session *sessions.Session) error {
	encoded, err := securecookie.EncodeMulti(session.Name(), session.Values, s.Codecs...)
	if err != nil {
		return err
	}

	return s.redis.Strings().Set_String(s.prefix+session.Name()+":"+session.ID, encoded)
}

func (s *RadixStore) loadSession(session *sessions.Session) error {
	encoded, err := s.redis.Strings().Get_String(s.prefix + session.Name() + ":" + session.ID)
	if err != nil {
		return err
	}

	return securecookie.DecodeMulti(session.Name(), encoded, &session.Values, s.Codecs...)
}

func (s *RadixStore) eraseSession(session *sessions.Session) error {
	_, err := s.redis.Keys().Del(s.prefix + session.Name())
	return err
}
