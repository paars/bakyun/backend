package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"

	"code.cloudfoundry.org/go-diodes"
	"github.com/rs/zerolog/diode"

	"github.com/fatih/color"
	"github.com/octago/sflags"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/shibukawa/configdir"
	bakyun "gitlab.com/paars/bakyun/backend"
	"gitlab.com/paars/bakyun/backend/config"
	"gitlab.com/paars/bakyun/backend/util"
	"gopkg.in/urfave/cli.v2"
)

var (
	configDir = configdir.New("paars", bakyun.ApplicationName)
	app       *cli.App
	cfg       = config.New()
)

const (
	moduleParam = "module"
	diodeSize   = 1000
)

func init() {
	dw := diode.NewWriter(color.Output, diodes.NewManyToOne(diodeSize, diodes.AlertFunc(func(missed int) {
		fmt.Printf("Logger Dropped %d messages", missed)
	})), 10*time.Millisecond)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: dw, NoColor: false})
	zerolog.SetGlobalLevel(zerolog.WarnLevel)
	util.FlagStringerFixer()

	app = &cli.App{
		HelpName:    bakyun.ApplicationName,
		Usage:       bakyun.ApplicationUsage,
		Version:     bakyun.Version,
		Description: bakyun.ApplicationDescription,

		EnableShellCompletion: true,

		Before: func(ctx *cli.Context) error {
			initLogging()

			cfg.Defaults()
			if err := cfg.Load(ctx.String("config")); err != nil {
				log.Warn().Err(err).Msg("failed to parse config file")
			}

			wg := new(sync.WaitGroup)

			wg.Add(2)
			go func() {
				if _, err := cfg.DB(); err != nil {
					log.Warn().Err(err).Str(moduleParam, "postgres").Msg("failed to connect")
				} else {
					log.Debug().Str(moduleParam, "postgres").Msg("connected")
				}

				wg.Done()
			}()

			go func() {
				if _, err := cfg.Redix(); err != nil {
					log.Warn().Err(err).Str(moduleParam, "redis").Msg("failed to connect")
				} else {
					log.Debug().Str(moduleParam, "redis").Msg("connected")
				}
				wg.Done()
			}()

			wg.Wait()

			log.Info().Str("version", bakyun.Version).Msgf("%s starting up!", color.HiBlueString(bakyun.Bakyun))

			return nil
		},

		After: func(ctx *cli.Context) error {
			log.Debug().Msg("shutting down")

			return nil
		},

		Writer:    color.Output,
		ErrWriter: color.Error,

		Metadata: make(map[string]interface{}),
	}
	app.Compiled, _ = time.Parse(time.RFC3339, bakyun.BuildDate)

	configDir.LocalPath, _ = filepath.Abs(".")
	// app.Authors
	// app.Copyright
	// app.Author
	// app.Email

	// app.CommandNotFound
	// app.OnUsageError

	flags, err := util.FlagsParse(cfg, sflags.EnvPrefix("BAKYUN_"))
	if err != nil {
		log.Fatal().Err(err).Msg("failed to parse flags")
	}

	app.Flags = append([]cli.Flag{
		&cli.StringFlag{
			Name:    "config",
			Aliases: []string{"c"},
			Usage:   "load specified configuration file",
			EnvVars: []string{"BAKYUN_CONFIG"},
		},
	}, flags...)
}

func initLogging() {
	if cfg.Verbose {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
}

// Execute the command line
func Execute() int {
	if err := app.Run(os.Args); err != nil && err != io.EOF {
		if me, ok := err.(*MessageError); ok {
			log.Error().Err(me.InnerError).Msg(me.Message)
			return me.ExitCode
		}
		log.Fatal().Err(err).Msg("Failed to execute, unexpected error!")
		return ExitUnkownError
	}

	return 0
}
