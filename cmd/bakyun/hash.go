package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/paars/bakyun/backend/util"
	cli "gopkg.in/urfave/cli.v2"
)

func init() {
	app.Commands = append(app.Commands, hashCommand)
}

var hashCommand = &cli.Command{
	Name:        "hash",
	Aliases:     []string{"generate-password", "hash-password"},
	Usage:       "Hash a password with the configured hashing format",
	Description: `Hash the password with the configured hashing algorithm`,

	Action: func(ctx *cli.Context) error {
		if ctx.NArg() < 1 {
			return NewExitMessageError(ExitMisc, "No password specofied")
		}

		rawPassword := ctx.Args().First()
		output, err := util.HashPassword(cfg.UCrypt.Algorithm, []byte(rawPassword), cfg.UCrypt.Configuration, 0)
		if err != nil {
			return NewExitMessageError(ExitMisc, "Failed to hash password").SetInnerError(err)
		}

		log.Info().Str("hash", output).Msg("successfully hashed the password")
		return nil
	},
}
