package main

import (
	"os"
)

func main() {
	exitCode := Execute()
	os.Exit(exitCode)
}
