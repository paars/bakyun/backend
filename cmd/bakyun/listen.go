package main

import (
	"context"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/paars/bakyun/backend"
	cli "gopkg.in/urfave/cli.v2"
)

func init() {
	app.Commands = append(app.Commands, listenCommand)
}

var listenCommand = &cli.Command{
	Name:        "listen",
	Aliases:     []string{"serve", "server"},
	Usage:       "Start the HTTP listener",
	Description: `This will start the HTTP server and listens for new connections`,

	Action: func(ctx *cli.Context) error {
		log.Debug().Msg("Launching server")
		errChan := make(chan error, 1)
		server, err := bakyun.Serve(cfg, nil, errChan)

		if err != nil {
			return NewExitMessageError(ExitHTTP, "Failed to initialize server").SetInnerError(err)
		}

		scheme := "http"
		if cfg.IsHTTPS() {
			scheme += "s"
		}
		u, err := url.Parse(scheme + "://" + server.Addr)
		if err != nil {
			return err
		}

		log.Info().Str("url", u.String()).Str("public", cfg.HTTP.Public).Msg("server started")

		sc := make(chan os.Signal, 1)
		signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill, syscall.SIGPIPE)

		for {
			select {
			case err := <-errChan:
				if err != nil && err != http.ErrServerClosed {
					return NewExitMessageError(ExitHTTP, "Error occured on listener").SetInnerError(err)
				}

				return nil
			case s := <-sc:
				log.Debug().Str("signal", s.String()).Msg("received signal")
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()
				server.Shutdown(ctx)
			}
		}
	},
}
