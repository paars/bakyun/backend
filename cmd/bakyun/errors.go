package main

// MessageError holds the friendly message error object
type MessageError struct {
	Message  string
	ExitCode int

	InnerError error
}

func (e *MessageError) Error() string {
	return e.Message
}

// SetInnerError allows setting of the real error
func (e *MessageError) SetInnerError(err error) *MessageError {
	e.InnerError = err
	return e
}

// NewMessageError create a new error message with exit code 0
func NewMessageError(msg string) *MessageError {
	return &MessageError{Message: msg}
}

// NewExitMessageError create a new error message with a custom exit code
func NewExitMessageError(exitCode int, msg string) *MessageError {
	return &MessageError{ExitCode: exitCode, Message: msg}
}
