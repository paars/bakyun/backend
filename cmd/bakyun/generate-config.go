package main

import (
	cli "gopkg.in/urfave/cli.v2"
)

func init() {
	app.Commands = append(app.Commands, generateConfigCommand)
}

var generateConfigCommand = &cli.Command{
	Name:  "generate-config",
	Usage: "Generate the configuration file",
	Description: `You can specify most options on the command line,
        but a configuration file saves a lot of repetitive work.
        Use this command to create a config file`,

	Action: func(ctx *cli.Context) error {
		if ctx.NArg() < 1 {
			return NewExitMessageError(ExitMissingConfigFile, "No config file specified")
		}

		filename := ctx.Args().First()
		err := cfg.Save(filename)

		if err != nil {
			err = NewExitMessageError(ExitMissingConfigFile, "Failed to save config").SetInnerError(err)
		}

		return err
	},
}
