package main

import (
	"flag"
	"path/filepath"
	"time"

	"github.com/rs/zerolog"

	bakyun "gitlab.com/paars/bakyun/backend"
	"gitlab.com/paars/bakyun/backend/config"
	"gitlab.com/paars/bakyun/backend/helper"
	"go.uber.org/fx"
)

var (
	verbose bool
)

const (
	diodeSize   = 1000
	moduleParam = "module"
)

func init() {
	flag.BoolVar(&verbose, "verbose", false, "Enable verbose logs")
}

type errHandler struct {
	log zerolog.Logger
}

func (eh *errHandler) HandleError(err error) {
	eh.log.Error().Err(err).Msg("unexpected error")
}

type runtimeOpts struct {
	fx.Out
	ConfigFile string `name:"configFile"`
}

func main() {
	flag.Parse()

	log := helper.NewLogger(diodeSize)
	fxLogger := log.With().Str("module", "fx").Logger()

	configFile := "./config.yaml"

	if flag.NArg() > 0 {
		configFile = flag.Arg(0)
	}

	configFile, err := filepath.Abs(configFile)
	if err != nil {
		panic(err)
	}

	app := fx.New(
		fx.ErrorHook(&errHandler{log: fxLogger}),
		fx.Logger(&fxLogger),

		fx.Provide(func() runtimeOpts {
			return runtimeOpts{
				ConfigFile: configFile,
			}
		}, func() zerolog.Logger {
			return log
		}),
		fx.Provide(config.Provider),
		fx.Invoke(func(log zerolog.Logger) {
			compiled, _ := time.Parse(time.RFC3339, bakyun.BuildDate)
			log.Info().
				Time("compiled", compiled).
				Str("version", bakyun.LongVersion).
				Msg("bakyun starting up")
		}),

		bakyun.HTTPModule,
	)
	app.Run()
	<-app.Done()
}
