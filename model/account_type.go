package model

// AccountType is the account_type enum
type AccountType string

// The different account types
const (
	AccountTypePerson       AccountType = "person"
	AccountTypeService                  = "service"
	AccountTypeGroup                    = "group"
	AccountTypeOrganization             = "organization"
)
