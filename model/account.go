package model

import (
	"errors"
	"net/url"

	"github.com/cking/reverse"

	"github.com/go-fed/activity/vocab"
	"github.com/go-pg/pg"
	"gitlab.com/paars/bakyun/backend/routes"
	"gitlab.com/paars/bakyun/plugins"
)

// Account model
type Account struct {
	ID          int
	Name        string
	DisplayName string
	Bio         string
	OwnerID     int
	Owner       *User
	Avatar      string
	AvatarMIME  string
	Banner      string
	Location    string
	PrivateKey  string
	PublicKey   string
	Type        AccountType `sql:"type:account_type"`
}

// AccountByName returns an Account instance with that name
func AccountByName(db *pg.DB, name string) (*Account, error) {
	m := new(Account)
	err := db.Model(m).Where("name = ?", name).Select(m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

// ResolveAvatar returns the resolved avatar url from the model
func (a *Account) ResolveAvatar(fp plugins.FileProvider) *url.URL {
	p, _ := fp.PublicURL(a.Avatar)
	return p
}

// Actor converts the current account model into a AS actor
func (a *Account) Actor(fp plugins.FileProvider, baseURL *url.URL) (actor vocab.ObjectType, err error) {
	switch a.Type {
	case AccountTypePerson:
		actor = new(vocab.Person)
	default:
		return nil, errors.New("not implemented")
	}

	actor.SetId(baseURL.ResolveReference(&url.URL{Path: routes.ActivityPubUserProfile(a.Name)}))
	actor.SetPreferredUsername(a.Name)

	displayName := a.Name
	if a.DisplayName != "" {
		displayName = a.DisplayName
	}
	actor.AppendNameString(displayName)
	actor.AppendSummaryString(a.Bio)

	if a.Location != "" {
		loc := &vocab.Place{}
		loc.AppendNameString(a.Location)
		// FIXME: lat/long/alt ? => https://www.w3.org/TR/activitystreams-vocabulary/#dfn-location
		actor.AppendLocationObject(loc)
	}

	actor.AppendUrlAnyURI(baseURL.ResolveReference(&url.URL{Path: routes.UserProfile(a.Name)}))

	if a.Avatar != "" {
		img := &vocab.Image{}
		pub, err := fp.PublicURL(a.Avatar)
		if err != nil {
			return nil, err
		}

		img.AppendUrlAnyURI(pub)
		actor.AppendIconImage(img)
	}

	if a.Banner != "" {
		img := &vocab.Image{}
		pub, err := fp.PublicURL(a.Banner)
		if err != nil {
			return nil, err
		}
		img.AppendUrlAnyURI(pub)
		actor.AppendImageImage(img)
	}

	// following
	// followers
	// liked
	// streams
	// endpoints
	//  oauthAuthorizationEndpoint
	//  oauthTokenEndpoint
	//  signClientKey
	//  sharedInbox
	// publicKey
	// isCat

	// tag <= holds custom emojis used

	actor.SetInboxAnyURI(baseURL.ResolveReference(&url.URL{Path: routes.ActivityPubUserProfileInbox(a.Name)}))
	actor.SetOutboxAnyURI(baseURL.ResolveReference(&url.URL{Path: routes.ActivityPubUserProfileOutbox(a.Name)}))
	actor.SetFollowersAnyURI(baseURL.ResolveReference(&url.URL{Path: routes.ActivityPubUserProfileFollower(a.Name)}))
	actor.SetFollowingAnyURI(baseURL.ResolveReference(&url.URL{Path: routes.ActivityPubUserProfileFollowing(a.Name)}))

	endpoints := &vocab.Object{}
	// endpoints.SetOauthAuthorizationEndpoint(util.MustURLParse(baseURL + "/oauth/authorize"))
	// endpoints.SetOauthTokenEndpoint(util.MustURLParse(baseURL + "/oauth/token"))
	endpoints.SetSharedInbox(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.ActivityPubSharedInbox)}))
	actor.SetEndpoints(endpoints)

	// web security
	//actor = ap.AddWebSecurityContext(a.PublicKey, actor)

	return actor, nil
}
