package model

import (
	"time"

	"github.com/go-pg/migrations"
	"github.com/rs/zerolog/log"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		queries := []string{
			`CREATE TYPE account_type AS ENUM ('person', 'service', 'group', 'organization')`,
			`CREATE TABLE "user" (
	id serial NOT NULL,
	username varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	CONSTRAINT user_pk PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
)`,
			`CREATE TABLE account (
	id serial NOT NULL,
	"name" varchar(255) NOT NULL,
	owner_id int4 NOT NULL,
	"type" account_type NOT NULL DEFAULT 'person'::account_type,
	CONSTRAINT account_pk PRIMARY KEY (id),
	CONSTRAINT account_un UNIQUE (name),
	CONSTRAINT account_user_fk FOREIGN KEY (owner_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
	OIDS=FALSE
)`,
			`CREATE UNIQUE INDEX account_name_idx ON public.account USING btree (name)`,
		}

		for _, query := range queries {
			start := time.Now()
			_, err := db.Exec(query)
			log.Info().Err(err).Dur("time", time.Now().Sub(start)).Str("sql", query).Msg("Executing query!")
			if err != nil {
				return err
			}
		}

		return nil
	}, func(db migrations.DB) error {
		queries := []string{
			`DROP TABLE account CASCADE`,
			`DROP TABLE "user" CASCADE`,
			`DROP TYPE account_type CASCADE`,
		}

		for _, query := range queries {
			start := time.Now()
			_, err := db.Exec(query)
			log.Info().Err(err).Dur("time", time.Now().Sub(start)).Str("sql", query).Msg("Executing query!")
			if err != nil {
				return err
			}
		}
		return nil
	})
}
