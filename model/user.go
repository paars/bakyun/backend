package model

import (
	"encoding/gob"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/rs/zerolog/log"
	"gitlab.com/paars/bakyun/backend/util"
)

func init() {
	gob.Register(new(User))
}

// User model
type User struct {
	ID       int
	Username string
	Password string
	Email    string

	Accounts []Account `pg:"fk:owner_id"`
}

// UserByNameOrEmail queries the database for matches for users matching username or email
func UserByNameOrEmail(db *pg.DB, id string) (*User, error) {
	u := new(User)
	err := db.Model(u).Where("username = ?", id).WhereOr("email = ?", id).Select(u)

	// no rows is no error, but no result as well
	if err == pg.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}
	return u, nil
}

// GetMainAccount gets the main (person) account for this user
func (u *User) GetMainAccount(db *pg.DB) (*Account, error) {
	a := new(Account)
	err := db.Model(u).Relation("account", func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("?TableAlias.type = ?", "person")
		return q, nil
	}).Select(a)
	if err != nil {
		return nil, err
	}

	return a, nil
}

// VerifyPassword checks if the given password matches the hashed one in the database
func (u *User) VerifyPassword(password string) bool {
	verified, err := util.VerifyPassword(u.Password, []byte(password))
	if err != nil {
		log.Error().Err(err).Msg("failed to verify passwords")
	}

	return verified
}
