package model

import (
	"flag"

	"github.com/go-pg/migrations"
	"github.com/go-pg/pg"
)

// Migrate runs all needed migrations
func Migrate(db *pg.DB) (oldVersion int64, newVersion int64, err error) {
	err = db.RunInTransaction(func(tx *pg.Tx) (err error) {
		oldVersion, newVersion, err = migrations.Run(tx, flag.Args()...)
		return
	})

	return
}
