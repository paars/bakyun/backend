package aproutes

// UserProfile is the path to the as user profile
func UserProfile(userName string) string {
	return "/users/" + userName
}

// UserProfileInbox is the path to the user profiles inbox
func UserProfileInbox(userName string) string {
	return UserProfile(userName) + "/inbox"
}

// UserProfileOutbox is the path to the user profiles outbox
func UserProfileOutbox(userName string) string {
	return UserProfile(userName) + "/outbox"
}

// UserProfileFollower is the path to the user profiles followers list
func UserProfileFollower(userName string) string {
	return UserProfile(userName) + "/follower"
}

// UserProfileFollowing is the path to the user profiles following list
func UserProfileFollowing(userName string) string {
	return UserProfile(userName) + "/following"
}

// UserProfilePage is the public user profile
func UserProfilePage(userName string) string {
	return "/@" + userName
}
