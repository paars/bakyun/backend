package middleware

type contextKey string

func (c contextKey) String() string {
	return "gitlab.com/paars/bakyun/backend/internal/middleware." + string(c)
}

var oauthKey = contextKey("oauth")
