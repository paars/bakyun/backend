package routing

import (
	"net/http"

	"gitlab.com/paars/bakyun/backend/config"

	"gitlab.com/paars/bakyun/backend/middlewares"

	"github.com/go-chi/chi"
)

func getAtUser(w http.ResponseWriter, r *http.Request) {
	cfg := config.FromRequest(r)
	if isActivityPubRequest(r) {
		http.Redirect(w, r, cfg.HTTP.Public+"/users/"+chi.URLParam(r, "username"), http.StatusMovedPermanently)
		return
	}

	w.Write([]byte(r.URL.String()))
}

func getUsersUser(w http.ResponseWriter, r *http.Request) {
	cfg := config.FromRequest(r)
	if !isActivityPubRequest(r) {
		http.Redirect(w, r, cfg.HTTP.Public+"/@"+chi.URLParam(r, "username"), http.StatusMovedPermanently)
		return
	}

	log := middlewares.GetZerologger(r)
	_, handler := getPubber(r.Context())

	if handled, err := handler(r.Context(), w, r); err != nil {
		w.Header().Set("Content-Type", "text/plain")
		log.Error().Err(err).Msg("failed to execute activity streams handler")
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else if handled {
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	http.Error(w, http.StatusText(http.StatusNoContent), http.StatusNoContent)
}
