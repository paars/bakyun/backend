package routing

import (
	"context"
	"net/http"
	"strings"

	"github.com/go-chi/chi/middleware"

	"github.com/go-fed/activity/pub"

	"github.com/go-chi/chi"
)

const (
	activityPubAcceptLD        = `application/ld+json`
	activityPubAcceptLDProfile = `https://www.w3.org/ns/activitystreams`
	activityPubAcceptActivity  = `application/activity+json`
)

type contextKey string

func (c contextKey) String() string {
	return "gitlab.com/paars/bakyun/backend/internal/routing." + string(c)
}

// Setup the router
func Setup(r chi.Router, pubber pub.Pubber, asHandler pub.HandlerFunc) {
	r.Get("/", getIndex)

	r.Get("/@{username}", getAtUser)

	r.Get("/login", getLogin)
	r.Post("/login", postLogin)

	// oauth 2
	r.Route("/oauth", func(r chi.Router) {
		r.HandleFunc("/authorize", oauthAuthorize)
	})

	// activity pub
	r.With(pubberMiddleware(pubber, asHandler), middleware.SetHeader("Content-Type", activityPubAcceptActivity)).Route("/users", func(r chi.Router) {
		r.Get("/{username}", getUsersUser)
	})

	// fallback to vue rendering
	r.Get("/*", func(w http.ResponseWriter, r *http.Request) {

	})
}

func getPubber(ctx context.Context) (pub.Pubber, pub.HandlerFunc) {
	pubber := ctx.Value(contextKey("pubber")).(pub.Pubber)
	asHandler := ctx.Value(contextKey("asHandler")).(pub.HandlerFunc)

	return pubber, asHandler
}

func pubberMiddleware(pubber pub.Pubber, asHandler pub.HandlerFunc) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			ctx = context.WithValue(ctx, contextKey("pubber"), pubber)
			ctx = context.WithValue(ctx, contextKey("asHandler"), asHandler)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func isActivityPubRequest(r *http.Request) bool {
	accept := r.Header.Get("accept")

	if accept == "" {
		return false
	}

	acceptParts := strings.Split(accept, ";")
	mediaType := strings.ToLower(strings.TrimSpace(acceptParts[0]))

	if mediaType == activityPubAcceptActivity && len(acceptParts) == 1 {
		return true
	}

	if mediaType != activityPubAcceptLD || len(acceptParts) < 2 {
		return false
	}

	profile := ""
	for _, part := range acceptParts[1:] {
		kv := strings.SplitN(part, "=", 2)
		if strings.TrimSpace(kv[0]) == "profile" {
			// unquote
			v := strings.Trim(strings.TrimSpace(kv[1]), "\"'")
			profile = v
			break
		}
	}

	return profile == activityPubAcceptLDProfile
}
