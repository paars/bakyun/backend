package routing

import (
	"net/http"

	"gitlab.com/paars/bakyun/frontend"
)

func getIndex(w http.ResponseWriter, _ *http.Request) {
	view, err := frontend.JetSet().GetTemplate("index.jet.html")
	if err != nil {
		panic(err)
	}

	view.Execute(w, nil, nil)
}
