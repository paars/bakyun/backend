package routing

import (
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/paars/bakyun/frontend"

	"gitlab.com/paars/bakyun/backend/config"
	"gitlab.com/paars/bakyun/backend/internal/model"
	"gitlab.com/paars/bakyun/backend/middlewares"

	"gitlab.com/paars/bakyun/backend/util/checkered"

	"github.com/gowww/check"

	"github.com/CloudyKit/jet"
)

const redirectParam = "to"

var (
	loginCheck = check.Checker{
		"login": {
			check.Required,
			check.MinLen(1),
			checkered.Or(
				check.Email,
				checkered.Regex(regexp.MustCompile(`^[-_.a-zA-Z0-9]+$`)),
			),
		},
		"password": {check.Required},
		// no need to check remember. nil/empty on false, filled on true
	}
)

func getLogin(w http.ResponseWriter, r *http.Request) {
	renderLoginForm(w, r, nil)
}

func postLogin(w http.ResponseWriter, r *http.Request) {
	sess, err := middlewares.SessionForRequest(r)
	if err != nil {
		errored(err, w, r)
		return
	}

	cfg := config.FromRequest(r)
	r.ParseForm()
	to := r.FormValue(redirectParam)
	if to == "" {
		to = "/"
	}

	errs := loginCheck.CheckRequest(r)
	if errs.NotEmpty() {
		renderLoginForm(w, r, errs)
		return
	}

	username := strings.TrimSpace(r.FormValue("login"))
	password := r.FormValue("password")
	rawRemember := r.FormValue("remember")
	remember, _ := strconv.ParseBool(rawRemember)

	user, err := model.UserByNameOrEmail(cfg.MustDB(), username)
	if err != nil {
		errored(err, w, r)
		return
	}

	if user == nil || !user.VerifyPassword(password) {
		sess.AddFlash("Could not login, make sure to use the correct username and password!", "form-error")
		errs.Add("_", &check.Error{
			Error: check.ErrInvalid,
		})
		renderLoginForm(w, r, errs)
		return
	}

	sess.Values["user"] = user
	if !remember {
		// make cookie vali
		sess.Options.MaxAge = 0
	}
	if err := sess.Save(r, w); err != nil {
		errored(err, w, r)
		return
	}

	redirect(w, r, to)
}

func renderLoginForm(w http.ResponseWriter, r *http.Request, errs check.Errors) {
	r.ParseForm()
	view, err := frontend.JetSet().GetTemplate("auth/login")
	if err != nil {
		errored(err, w, r)
		return
	}

	vm := make(jet.VarMap)
	if errs != nil {
		vm.Set("errors", errs)
	}
	view.Execute(w, vm, r.Form)
}
