package routing

import (
	"net/http"
	"net/url"

	"gitlab.com/paars/bakyun/backend/middlewares"

	"github.com/RangelReale/osin"
	"github.com/rs/zerolog/log"
	"gitlab.com/paars/bakyun/backend/internal/middleware"
	"gitlab.com/paars/bakyun/backend/internal/model"
)

func oauthAuthorize(w http.ResponseWriter, r *http.Request) {
	server := middleware.OAuthFromRequest(r)
	resp := server.NewResponse()
	sess, err := middlewares.SessionForRequest(r)

	if err != nil {
		errored(err, w, r)
		return
	}

	defer resp.Close()

	if ar := server.HandleAuthorizeRequest(resp, r); ar != nil {
		rawUser, ok := sess.Values["user"].(*model.User)
		if !ok || rawUser == nil {
			escapedURI := url.QueryEscape(r.RequestURI)
			redirect(w, r, "/login?to="+escapedURI, http.StatusTemporaryRedirect)
			return
		}

		ar.Authorized = true
		server.FinishAuthorizeRequest(resp, r, ar)
	}

	if resp.IsError && resp.InternalError != nil {
		log.Error().Err(resp.InternalError).Msg("OAuth error")
	}

	osin.OutputJSON(resp, w, r)
}
