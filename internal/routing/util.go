package routing

import (
	"fmt"
	"net/http"

	"gitlab.com/paars/bakyun/backend/config"
	"gitlab.com/paars/bakyun/backend/middlewares"
	"gitlab.com/paars/bakyun/frontend"

	"github.com/CloudyKit/jet"
	"github.com/pkg/errors"
)

func errored(err error, w http.ResponseWriter, r *http.Request) {
	log := middlewares.GetZerologger(r)
	log.Error().Err(err).Msg("failed to execute request")

	errorText := fmt.Sprintf("%+v", errors.WithStack(err))

	view, vfsErr := frontend.JetSet().GetTemplate("error/500")
	if vfsErr != nil {
		log.Error().Err(vfsErr).Msg("failed fetch error template")
		panic(vfsErr)
	}

	w.WriteHeader(http.StatusInternalServerError)

	vm := make(jet.VarMap)
	vm.Set("error", err)
	vm.Set("errorText", errorText)
	view.Execute(w, vm, r)
}

func redirect(w http.ResponseWriter, r *http.Request, url string, statusCode ...int) {
	redirectStatusCode := http.StatusFound
	if len(statusCode) > 0 {
		redirectStatusCode = statusCode[0]
	}

	cfg := config.FromRequest(r)

	http.Redirect(w, r, cfg.HTTP.Public+url, redirectStatusCode)
}
