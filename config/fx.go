package config

import (
	"github.com/rs/zerolog"
	"go.uber.org/fx"
)

type providerOpts struct {
	fx.In
	ConfigFile string `name:"configFile" optional:"true"`
}

func Provider(opts providerOpts, log zerolog.Logger) *Config {
	c := New()

	if opts.ConfigFile != "" {
		if err := c.Load(opts.ConfigFile); err != nil {
			log.Warn().Err(err).Msg("failed to read config file. continuing with default settings...")
		}
	}

	return c
}
