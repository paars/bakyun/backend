package webfinger

import (
	"net/http"
	"net/url"

	"github.com/go-chi/chi"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"gitlab.com/paars/bakyun/backend/cacher"
	"gitlab.com/paars/bakyun/backend/config"
	"gitlab.com/paars/bakyun/plugins"
	"go.uber.org/fx"
)

type out struct {
	fx.Out

	Route http.Handler `group:"routes"`
}

func Provider(logger zerolog.Logger, cfg *config.Config, router chi.Router, cacher *cacher.Cacher, pg *pg.DB, fp plugins.FileProvider) (out out, err error) {
	log := logger.With().Str("module", "webfinger").Logger()
	var baseURL *url.URL
	baseURL, err = url.Parse(cfg.HTTP.Public)
	if err != nil {
		return
	}

	// force a value on route
	out.Route = router

	resolver := NewResolver(log, fp, baseURL, cacher, pg)
	routes := NewRoutes(resolver)
	routes.Register(router)

	return
}
