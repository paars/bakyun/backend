package webfinger

import (
	"net/url"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/paars/bakyun/backend/cacher"
	"gitlab.com/paars/bakyun/backend/model"
	"gitlab.com/paars/bakyun/backend/routes"
	"gitlab.com/paars/bakyun/plugins"
	"gitlab.com/paars/bakyun/webfinger/jrd"
	webfinger "gitlab.com/paars/bakyun/webfinger/server"
)

// Resolver resolves webfinger requests
type Resolver struct {
	host   *url.URL
	cacher *cacher.Cacher
	db     *pg.DB
	log    zerolog.Logger
	fp     plugins.FileProvider
}

// NewResolver creates a new webfinger resolver
func NewResolver(log zerolog.Logger, fp plugins.FileProvider, host *url.URL, cacher *cacher.Cacher, db *pg.DB) webfinger.Resolver {
	resolver := &Resolver{
		host:   host,
		cacher: cacher,
		db:     db,
		log:    log,
		fp:     fp,
	}

	return resolver
}

// FindURI finds the resource for the given URI
func (r *Resolver) FindURI(uri *url.URL, rels []string) (*jrd.JRD, error) {
	log.Debug().Str("uri", uri.String()).Msg("incoming finger uri request")
	host := uri.Hostname()

	segments := strings.Split(uri.RawPath, "/")
	uname := segments[len(segments)-1]

	switch uri.RawPath {
	case routes.UserProfile(uname),
		routes.ActivityPubUserProfile(uname):
		return r.FindUser(uname, host, rels)
	default:
		return nil, webfinger.ErrNotFound
	}
}

// FindUser finds the user given the username and hostname.
func (r *Resolver) FindUser(username, hostname string, _ []string) (res *jrd.JRD, err error) {
	r.log.Debug().Str("acct", username+"@"+hostname).Msg("incoming finger acct request")

	if hostname != r.host.Host {
		return nil, webfinger.ErrNotFound
	}

	start := time.Now()
	defer func() {
		r.log.Debug().Dur("runtime", time.Now().Sub(start)).Msg("finger request query time")
	}()

	// query the database
	rawModel, err := r.cacher.Fetch("model:"+username, func() (interface{}, error) {
		return model.AccountByName(r.db, username)
	})
	if err != nil {
		r.log.Warn().Err(err).Msg("failed to query user!")
		return nil, err
	}
	model, ok := rawModel.(*model.Account)
	if !ok {
		return nil, webfinger.ErrNotFound
	}

	// now build the resource
	res = new(jrd.JRD)

	res.Subject = "acct:" + username + "@" + hostname
	res.Aliases = []string{
		r.host.ResolveReference(&url.URL{Path: routes.UserProfile(username)}).String(),            // human readable end point
		r.host.ResolveReference(&url.URL{Path: routes.ActivityPubUserProfile(username)}).String(), // machine readable (activity stream) endpoint
	}

	res.Links = []jrd.Link{
		jrd.Link{
			Rel:  RelProfilePage,
			Href: res.Aliases[0],
			Type: TypeProfilePage,
		},
		jrd.Link{
			Rel:  RelAvatar,
			Href: model.ResolveAvatar(r.fp).String(),
			Type: model.AvatarMIME,
		},
		jrd.Link{
			Rel:  RelSelf,
			Href: res.Aliases[1],
			Type: TypeSelf,
		},
		jrd.Link{
			Rel:      RelSubscribe,
			Template: r.host.ResolveReference(&url.URL{Path: routes.OStatusSubscribe(), RawQuery: "account={uri}"}).String(),
		},
	}
	return res, nil
}
