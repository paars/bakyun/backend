package webfinger

import (
	"github.com/go-chi/chi"
	webfinger "gitlab.com/paars/bakyun/webfinger/server"
)

// Routes setups the resolver routes
type Routes struct {
	resolver webfinger.Resolver
}

// NewRoutes creates a new routes struct
func NewRoutes(resolver webfinger.Resolver) *Routes {
	return &Routes{
		resolver: resolver,
	}
}

// Register all webfinger routes
func (r *Routes) Register(router chi.Router) {
	wf := webfinger.Default(r.resolver)

	// we probably work with a reverse proxy, which does the https magic
	wf.AllowHTTP = true

	router.Handle(webfinger.WebFingerPath, wf)
}
