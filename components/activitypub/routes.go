package activitypub

import (
	"net/http"

	"github.com/cking/reverse"
	"github.com/go-chi/chi"
	"github.com/go-fed/activity/pub"
	"gitlab.com/paars/bakyun/backend/middleware"
)

const (
	activityPubAcceptLD        = "application/ld+json"
	activityPubAcceptLDProfile = "https://www.w3.org/ns/activitystreams"
	activityPubAcceptActivity  = "application/activity+json"
)

type Routes struct {
	pubber  pub.Pubber
	handler pub.HandlerFunc
}

func NewRoutes(pubber pub.Pubber, handler pub.HandlerFunc) *Routes {
	return &Routes{
		pubber:  pubber,
		handler: handler,
	}
}

func (rs *Routes) Register(router chi.Router) {
	router.Get(reverse.Get("ap-users-profile"), rs.getActivityPubUser)
}

func (rs *Routes) getActivityPubUser(w http.ResponseWriter, r *http.Request) {
	log := middleware.GetZerologger(r)

	if handled, err := rs.handler(r.Context(), w, r); err != nil {
		w.Header().Set("Content-Type", "text/plain")
		log.Error().Err(err).Msg("failed to execute activity streams handler")
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else if handled {
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	http.Error(w, http.StatusText(http.StatusNoContent), http.StatusNoContent)
}
