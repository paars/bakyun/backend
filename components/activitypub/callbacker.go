package activitypub

import (
	"context"

	"github.com/go-fed/activity/streams"
)

type Callbacker struct{}

func (cb *Callbacker) Create(c context.Context, s *streams.Create) error {
	panic("Create not implemented")
}

func (cb *Callbacker) Update(c context.Context, s *streams.Update) error {
	panic("Update not implemented")
}

func (cb *Callbacker) Delete(c context.Context, s *streams.Delete) error {
	panic("Delete not implemented")
}

func (cb *Callbacker) Add(c context.Context, s *streams.Add) error {
	panic("Add not implemented")
}

func (cb *Callbacker) Remove(c context.Context, s *streams.Remove) error {
	panic("Remove not implemented")
}

func (cb *Callbacker) Like(c context.Context, s *streams.Like) error {
	panic("Like not implemented")
}

func (cb *Callbacker) Block(c context.Context, s *streams.Block) error {
	panic("Block not implemented")
}

func (cb *Callbacker) Follow(c context.Context, s *streams.Follow) error {
	panic("Follow not implemented")
}

func (cb *Callbacker) Undo(c context.Context, s *streams.Undo) error {
	panic("Undo not implemented")
}

func (cb *Callbacker) Accept(c context.Context, s *streams.Accept) error {
	panic("Accept not implemented")
}

func (cb *Callbacker) Reject(c context.Context, s *streams.Reject) error {
	panic("Reject not implemented")
}
