package activitypub

import "time"

// Clock is to implement pub.Clock
type Clock struct{}

// Now is pub.Clock.Now
func (*Clock) Now() time.Time {
	return time.Now()
}
