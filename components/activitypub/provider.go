package activitypub

import (
	"net/http"
	"net/url"

	"gitlab.com/paars/bakyun/plugins"

	"github.com/go-chi/chi"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"gitlab.com/paars/bakyun/backend/cacher"
	"gitlab.com/paars/bakyun/backend/config"
	"go.uber.org/fx"
)

type in struct {
	fx.In

	ApplicationName string `name:"ApplicationName"`
	Logger          zerolog.Logger
	Config          *config.Config
	Router          chi.Router
	Cacher          *cacher.Cacher
	PG              *pg.DB
	FP              plugins.FileProvider
}

type Out struct {
	fx.Out

	Route http.Handler `group:"routes"`
}

func Provider(in in) (out Out, err error) {
	log := in.Logger.With().Str("module", "activitypub").Logger()
	var baseURL *url.URL
	baseURL, err = url.Parse(in.Config.HTTP.Public)
	if err != nil {
		return
	}

	// force a value on route
	out.Route = in.Router

	// TODO: replace 10[delivery retries], 5[delivery depth], 5[forwarding depth] with configurable options
	apRoutes := NewRoutes(Defaults(log, in.Cacher, in.PG, baseURL, in.FP, in.ApplicationName, 10, 5, 5))
	apRoutes.Register(in.Router)

	return
}
