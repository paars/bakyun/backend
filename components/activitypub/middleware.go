package activitypub

import (
	"mime"
	"net/http"

	"github.com/rs/zerolog/log"
)

func activityPubMiddleware(router http.Handler) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			accept := r.Header.Get("accept")
			log.Debug().Str("accept", accept).Msg("header")
			mimeType, props, err := mime.ParseMediaType(accept)
			if err != nil {
				if mimeType == activityPubAcceptLD || (mimeType == activityPubAcceptActivity && props["profile"] == activityPubAcceptLDProfile) {
					router.ServeHTTP(w, r)
					return
				}
			}

			next.ServeHTTP(w, r)
		})
	}
}
