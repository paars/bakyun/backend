package activitypub

import (
	"net/url"
	"time"

	"golang.org/x/time/rate"

	"github.com/go-fed/activity/deliverer"
)

type DeliveryPersister struct {
}

func (p *DeliveryPersister) Sending(b []byte, to *url.URL) string {
	panic("Sending not implemented")
}

func (p *DeliveryPersister) Cancel(id string) {
	panic("Cancel not implemented")
}

func (p *DeliveryPersister) Successful(id string) {
	panic("Successful not implemented")
}

func (p *DeliveryPersister) Retrying(id string) {
	panic("Retrying not implemented")
}

func (p *DeliveryPersister) Undeliverable(id string) {
	panic("Undeliverable not implemented")
}

func NewDeliverer(persister deliverer.DeliveryPersister, rateLimiter *rate.Limiter, deliveryRetries int) *deliverer.DelivererPool {
	return deliverer.NewDelivererPool(deliverer.DeliveryOptions{
		InitialRetryTime: connectionTimeout,
		MaximumRetryTime: connectionTimeout * time.Duration(deliveryRetries),
		BackoffFactor:    backoffFactor,
		MaxRetries:       deliveryRetries,
		RateLimit:        rateLimiter,
		Persister:        persister,
	})
}

func NewDeliveryPersister() deliverer.DeliveryPersister {
	return &DeliveryPersister{}
}
