package cacher

import (
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/paars/bakyun/backend/cacher"
	"go.uber.org/fx"
)

type In struct {
	fx.In

	ApplicationName string `name:"ApplicationName"`
}

func Provider(in In, redis *redis.Pool) *cacher.Cacher {
	return cacher.New(redis, 5*time.Minute, in.ApplicationName)
}
