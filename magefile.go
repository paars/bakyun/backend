//+build mage

package main

import (
	"io/ioutil"
	"path/filepath"
	"runtime"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/target"

	"gitlab.com/paars/bakyun/magician"
)

var (
	isWin   = runtime.GOOS == "windows"
	Default = BuildDev
)

func BuildDev() error {
	magician.Headline("Build [Dev]")

	exe := "out/debug"
	if isWin {
		exe += ".exe"
	}

	files, err := ioutil.ReadDir(".")
	var paths []string
	for _, f := range files {
		if f.IsDir() && (f.Name() == "out" || f.Name()[0] == byte('.')) {
			continue
		}

		if !f.IsDir() && filepath.Ext(f.Name()) != ".go" {
			continue
		}

		paths = append(paths, f.Name())
	}

	build, err := target.Dir(exe, paths...)
	if err != nil {
		return err
	}

	if !build {
		magician.Echo("skipping, already up to date!")
		return nil
	}

	args := []string{
		"build", "-tags", "dev debug", "-race",
		"-o", exe,
		"-ldflags", magician.GitVersionLDFlags(magician.GetGoPackageName(".")),
	}
	if mg.Verbose() {
		args = append(args, "-v")
	}

	args = append(args, "./cmd/bakyund")

	return magician.Exec("go", args...)
}

func ServeDev() error {
	mg.SerialDeps(BuildDev)
	magician.Headline("Serve [Dev]")

	return magician.ExecEx(nil, "out", "debug", "--verbose")
}

func Delve() error {
	magician.Headline("Delve")

	return magician.ExecEx(
		nil, ".",
		"dlv", "debug",
		"--build-flags", "-tags 'dev debug'",
		"--wd", "out",
		"./cmd/bakyund", "--",
		"--verbose",
	)
}
