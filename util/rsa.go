package util

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

// from https://en.wikipedia.org/wiki/Key_size#Asymmetric_algorithm_key_lengths
// RSA claims that 1024-bit keys are likely to become crackable some time between
// 2006 and 2010 and that 2048-bit keys are sufficient until 2030.
// The NIST recommends 2048-bit keys for RSA.
const rsaKeySize = 2048

// The different RSA Key types
const (
	RSAPrivateKey = "PRIVATE KEY"
	RSAPublicKey  = "PUBLIC KEY"
)

// GenerateRSAKeys creates a private and public key in pem format
// Based on https://gist.github.com/sdorra/1c95de8cb80da31610d2ad767cd6f251
func GenerateRSAKeys() ([]byte, []byte, error) {
	key, err := rsa.GenerateKey(rand.Reader, rsaKeySize)
	if err != nil {
		return nil, nil, err
	}

	priv := &pem.Block{
		Type:  RSAPrivateKey,
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}

	pub := &pem.Block{
		Type:  RSAPublicKey,
		Bytes: x509.MarshalPKCS1PublicKey(&key.PublicKey),
	}

	return pem.EncodeToMemory(priv), pem.EncodeToMemory(pub), nil
}
