package util

import (
	"strconv"
	"time"

	"github.com/mediocregopher/radix.v3"
)

// DefaultPrefix is the default prefix for the Radix Store
const DefaultPrefix = "scs:session:"
const decimal = 10

// RadixStore represents the currently configured session session store.
type RadixStore struct {
	redis  radix.Client
	prefix string
}

// NewStore returns a new RadixStore instance.
func NewStore(redis radix.Client) *RadixStore {
	return NewStoreWithPrefix(redis, DefaultPrefix)
}

// NewStoreWithPrefix returns a new RadixStore instance with a custom query prefix.
func NewStoreWithPrefix(redis radix.Client, prefix string) *RadixStore {
	return &RadixStore{redis: redis, prefix: prefix}
}

// Find returns the data for a given session token from the RadixStore instance. If the session
// token is not found or is expired, the returned exists flag will be set to false.
func (r *RadixStore) Find(token string) ([]byte, bool, error) {
	var b []byte

	err := r.redis.Do(radix.Cmd(&b, "GET", r.prefix+token))
	if len(b) == 0 && err == nil {
		return nil, false, nil
	} else if err != nil {
		return nil, false, err
	}

	return b, true, nil
}

// Save adds a session token and data to the RadixStore instance with the given expiry time.
// If the session token already exists then the data and expiry time are updated.
func (r *RadixStore) Save(token string, b []byte, expiry time.Time) error {
	p := radix.Pipeline(
		radix.Cmd(nil, "MULTI"),
		radix.Cmd(nil, "SET", r.prefix+token, string(b)),
		radix.Cmd(nil, "PEXPIREAT", r.prefix+token, strconv.FormatInt(makeMillisecondTimestamp(expiry), decimal)),
		radix.Cmd(nil, "EXEC"),
	)

	return r.redis.Do(p)
}

// Delete removes a session token and corresponding data from the ResisStore instance.
func (r *RadixStore) Delete(token string) error {
	err := r.redis.Do(radix.Cmd(nil, "DEL", r.prefix+token))
	return err
}

func makeMillisecondTimestamp(t time.Time) int64 {
	return t.UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
}
