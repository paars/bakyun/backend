package checkered

import (
	"mime/multipart"
	"regexp"

	"github.com/gowww/check"
	"golang.org/x/text/language"
)

// ErrNoRegex is the error id if the regex doesn't match
var ErrNoRegex = &check.ErrorID{ID: "noRegex", Locales: map[language.Tag]string{
	language.English: "The required expression didn't match.",
}}

// Regex checks the form value by the given regular expression
func Regex(r *regexp.Regexp) check.Rule {
	return func(errs check.Errors, form *multipart.Form, key string) {
		vs := form.Value[key]
		switch len(vs) {
		case 0:
			errs.Add(key, &check.Error{Error: ErrNoRegex})
		default:
			v := vs[0]
			if !r.MatchString(v) {
				errs.Add(key, &check.Error{Error: ErrNoRegex})
			}
		}
	}
}
