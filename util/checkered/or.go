package checkered

import (
	"mime/multipart"

	"github.com/gowww/check"
	"golang.org/x/text/language"
)

// ErrNoOrMatch is the error id if none of the given rules match
var ErrNoOrMatch = &check.ErrorID{ID: "noOr", Locales: map[language.Tag]string{
	language.English: "No rule matched.",
}}

// Or uses the given list of rules and checks if at least one of them matches
func Or(rules ...check.Rule) check.Rule {
	return func(errs check.Errors, form *multipart.Form, key string) {
		for _, r := range rules {
			ruleErrs := make(check.Errors)

			r(ruleErrs, form, key)

			if ruleErrs.Empty() {
				return
			}
		}

		errs.Add(key, &check.Error{Error: ErrNoOrMatch})
	}
}
