package bakyun

import (
	"net/http"

	"gitlab.com/paars/bakyun/backend/components/activitypub"
	"gitlab.com/paars/bakyun/plugins"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/rs/zerolog"
	"gitlab.com/paars/bakyun/backend/components/cacher"
	"gitlab.com/paars/bakyun/backend/components/postgres"
	"gitlab.com/paars/bakyun/backend/components/redis"
	"gitlab.com/paars/bakyun/backend/components/webfinger"
	"gitlab.com/paars/bakyun/backend/config"
	mw "gitlab.com/paars/bakyun/backend/middleware"
	"go.uber.org/fx"
)

const corsMaxAge = 300

var (
	HTTPModule = fx.Options(
		plugins.HostProvider,
		PluginProvider,

		fx.Provide(
			Globals,
			postgres.Provider,
			redis.Provider,
			cacher.Provider,
			ProvideRouter,
			webfinger.Provider,
			activitypub.Provider,
		),

		fx.Invoke(Serve),
	)
)

func ProvideRouter(logger zerolog.Logger) chi.Router {
	log := logger.With().Str("module", "http").Logger()
	router := chi.NewRouter()

	// catch panics
	router.Use(middleware.Recoverer)

	// logging relevant data structures
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(mw.NewZeroLogger(log))

	// security
	router.Use(cors.New(cors.Options{
		AllowCredentials: false,
		AllowedMethods:   []string{"GET", "HEAD", "OPTIONS", "POST", "PUT", "DELETE", "PATCH"},
		AllowedOrigins:   []string{"*"}, // Federation must support all origins
		MaxAge:           corsMaxAge,
	}).Handler)

	// forward head if needed
	router.Use(middleware.GetHead)

	/*
		// TODO: replace 10[delivery retries], 5[delivery depth], 5[forwarding depth] with configurable options
		apRoutes := activitypub.NewRoutes(activitypub.Defaults(log.With().Str("module", "activitypub").Logger(), cacher, postgres, publicURL, fp, ApplicationName, 10, 5, 5))
		apRoutes.Register(router)
	*/

	// session := helper.NewRadixStore(ApplicationName+":session:", ApplicationName, cfg.MustRedix(), cfg.Cookie.GetByteKeys()...)
	//router.Use(helper.SessionStoreMiddleware(session))

	/*
		jet := frontend.JetSet()

		ConfigureJet(cfg)

		r.Use(cfg.Middleware)
		r.Use(imw.OAuthMiddleware(oauth))

		pubber, asHandler := ActivityPubber(cfg)
		routing.Setup(r, pubber, asHandler)

		if cfg.HTTP.Static != "" {
			fs := http.FileServer(http.Dir(cfg.HTTP.Static))
			r.Handle("/*", fs)
		}

		router.Handle("/*", frontend.Assets())
	*/

	return router
}

// ConfigureJet adds global variables to jet
// func ConfigureJet(cfg *config.Config) {
// 	if jetInited {
// 		return
// 	}
// 	jetInited = true

// 	frontend.JetSet().AddGlobal("Vars", cfg.Variables)
// 	frontend.JetSet().AddGlobal("BaseURL", cfg.HTTP.Public)
// }

type in struct {
	fx.In

	Log    zerolog.Logger
	Config *config.Config
	Router chi.Router

	// handler register themselves, no need to intervene
	Handler []http.Handler `group:"routes"`
}

func Serve(in in) error {
	routes := in.Router.Routes()
	in.Log.Debug().Int("routes", len(routes)).Msg("registered routes")
	for _, r := range routes {
		in.Log.Debug().Str("route", r.Pattern).Msg("found route")
	}

	h := &http.Server{
		Addr:    in.Config.HTTP.Listen,
		Handler: in.Router,
	}

	errChan := make(chan error)

	go func() {
		var err error

		if in.Config.HTTP.SSL.Cert != "" && in.Config.HTTP.SSL.Key != "" {
			err = h.ListenAndServeTLS(in.Config.HTTP.SSL.Cert, in.Config.HTTP.SSL.Key)
		} else {
			err = h.ListenAndServe()
		}

		errChan <- err
	}()

	in.Log.Info().Str("listen", in.Config.HTTP.Listen).Msg("server started")

	return <-errChan
}
