package bakyun

import (
	"go.uber.org/fx"
)

// application definition
const (
	ApplicationName = "bakyun"
)

// provided by buildscript
var (
	GitCommit   = "unknown"
	GitBranch   = "none"
	GitState    = "dirty"
	GitSummary  = "dirty"
	BuildDate   = "now"
	Version     = "0.0.0"
	LongVersion = "0.0.0-dirty"
)

type Global struct {
	fx.Out

	ApplicationName string `name:"ApplicationName"`
	GitCommit       string `name:"GitCommit"`
	GitBranch       string `name:"GitBranch"`
	GitState        string `name:"GitState"`
	GitSummary      string `name:"GitSummary"`
	BuildDate       string `name:"BuildDate"`
	Version         string `name:"Version"`
	LongVersion     string `name:"LongVersion"`
}

func Globals() Global {
	return Global{
		ApplicationName: ApplicationName,
		GitCommit:       GitCommit,
		GitBranch:       GitBranch,
		GitState:        GitState,
		GitSummary:      GitSummary,
		BuildDate:       BuildDate,
		Version:         Version,
		LongVersion:     LongVersion,
	}
}
