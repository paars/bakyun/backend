package bakyun

import (
	"gitlab.com/paars/bakyun/backend/config"
	"gitlab.com/paars/bakyun/plugins"
	"go.uber.org/fx"
)

var (
	host = newRegistry()

	PluginProvider = fx.Provide(
		host.ProvideFileProvider,
	)
)

func init() {
	plugins.Host = host

	plugins.Host.RegisterFileProvider("file", &plugins.LocalFileProvider{})
}

type registry struct {
	fileProviders map[string]plugins.FileProvider
}

func newRegistry() *registry {
	r := new(registry)
	r.fileProviders = make(map[string]plugins.FileProvider)
	return r
}

func (r *registry) RegisterFileProvider(id string, provider plugins.FileProvider) error {
	if _, existing := r.fileProviders[id]; existing {
		return plugins.ErrIDAlreadyRegistered
	}

	r.fileProviders[id] = provider
	return nil
}

func (r *registry) FileProvider(id string) (plugins.FileProvider, error) {
	fp, ok := r.fileProviders[id]
	if !ok {
		return nil, plugins.ErrIDNotFound
	}
	return fp, nil
}

func (r *registry) ProvideFileProvider(cfg *config.Config) (plugins.FileProvider, error) {
	fp, err := r.FileProvider(cfg.Store.Provider)
	if err != nil {
		return nil, err
	}

	fp.Initialize(cfg.Store.Configuration)
	return fp, nil
}
